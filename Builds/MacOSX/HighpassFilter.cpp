//
//  HighpassFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#include "HighpassFilter.h"


HighpassFilter::HighpassFilter()
{
    setDelayInSamples(1.f);
}

HighpassFilter::~HighpassFilter()
{
    
}

//==============================================================================

float HighpassFilter::filter(float input)
{
    float delayLineOutput = delayLineRead();
    
    float output = (feedbackGain * input) + ((1.f-feedbackGain) * delayLineOutput);
    
    delayLineWrite(output);
    
    return output * -1;
}
