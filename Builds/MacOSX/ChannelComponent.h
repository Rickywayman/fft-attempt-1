//
//  ChannelComponent.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 10/01/2016.
//
//

#ifndef __JuceBasicWindow__ChannelComponent__
#define __JuceBasicWindow__ChannelComponent__

#include "../JuceLibraryCode/JuceHeader.h"
#include "MainComponent.h"
#include <stdio.h>

//==============================================================================
/*
 This component lives inside our window, and this is where you should put all
 your controls and content.
 */
class ChannelComponent   : public Component
                       // public AudioDeviceManager, //inherit from audiodevicemanager
                        //public AudioIODeviceCallback, //inherit from audioIOdeivcecallback
                        //public SliderListener
                        //public AudioDeviceSelectorComponent
                        //public DocumentWindow

{
    /* */
public:
    //==============================================================================
    
    /* */
    ChannelComponent();
    
    
    void closeButtonPressed()
    {
        delete this;
    }
    /* */
    ~ChannelComponent();
    
    /* */
    //void resized() override;
    
    /* override the pure virtual funtions */
    /*void audioDeviceIOCallback(const float** inputChannelData,
                               int numInputChannels,
                               float** outputChannelData,
                               int numOutputChannels,
                               int numSamples) override;
    */
    /* */
    //void audioDeviceAboutToStart(AudioIODevice* device) override;
    /* */
   // void audioDeviceStopped() override;
    
    /* */
    //void sliderValueChanged	(Slider * 	slider1);
    
    //void resized(AudioDeviceSelectorComponent* settings);
    
    //void timerCallback(AudioDeviceSelectorComponent* settings);
    
    /* */
private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ChannelComponent)
    /* *///
    //AudioDeviceManager audioDeviceManager;
    //AudioDeviceSelectorComponent settings;
    //Slider slider1;
    //DocumentWindow  channel1;
    
};



#endif /* defined(__JuceBasicWindow__ChannelComponent__) */
